<?php
/*
Plugin Name: GRE Custom Dashboard
Plugin URI: www.spindox.it
Description: Customizes the WordPress Dashboard.
Version: 1.0
Author: Spindox S.p.A.
Author URI: www.spindox.it
*/

define( 'GRE_CUSTOMDASHBOARD_PATH', plugin_dir_path(__FILE__) );
define( 'GRE_CUSTOMDASHBOARD_URL', plugin_dir_url(__FILE__) );

class GreCustomDashboard{
    
    /*** Refers to a single instance of this class. ***/
    private static $instance = null;
    
    const PLUGIN_TEXT_DOMAIN = 'grecustomdashboard';
    
    /**
     * Creates or returns an instance of this class.
     * @return GreCustomDashboard a single instance of this class.
     */
    public static function get_instance(){
        if ( null == self::$instance ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Initializes the plugin by setting localization, filters, and administration functions.
     */
    private function __construct() {
        $class_name = get_class();
        register_activation_hook(__FILE__, array( $class_name, 'on_plugin_activation') );
        register_deactivation_hook(__FILE__, array( $class_name, 'on_plugin_deactivation'));
        self::register_hook_callbacks();
        self::load_plugin_textdomain();
    }
    
    /**
     * Register callbacks for actions and filters
     */
    public static function register_hook_callbacks(){
        $class_name = get_class();
        add_action( 'wp_dashboard_setup', array( $class_name, 'remove_dashboard_widgets' ) );
        add_action( 'wp_before_admin_bar_render' , array( $class_name, 'remove_admin_bar_links' ) );
        add_action( 'admin_menu', array( $class_name, 'remove_admin_menu_items' ), 999 );
        add_action( 'admin_menu', array( $class_name, 'add_admin_menu_link_all_options' ) );
        add_action( 'widgets_init', array( $class_name, 'remove_default_widgets' ) );
        add_filter( 'the_generator', array( $class_name, 'remove_wp_version' ) );
        add_action( 'dashboard_glance_items', array( $class_name, 'custom_dashboard_glance_items'));
        add_action( 'admin_enqueue_scripts', array( $class_name, 'register_css_js_admin') );
        add_filter( 'acf/settings/show_admin', array( $class_name, 'hide_acf_menu') );
        add_filter( 'admin_footer_text', array( $class_name, 'change_footer_text_admin' ) );
        add_filter( 'wp_kses_allowed_html', array( $class_name, 'allow_iframe_tags'), 1, 2 );
        add_action( 'login_head', array( $class_name, 'custom_login_head'));
        add_filter( 'manage_pages_columns', array( $class_name, 'edit_columns_head_pages'));
        add_action( 'manage_pages_custom_column', array( $class_name, 'manage_columns_content_pages'), 10, 2 );
        add_filter( 'login_headerurl', array( $class_name, 'change_login_logo_url'));
        add_filter( 'login_headertitle', array( $class_name, 'change_login_logo_url_title'));
    }
    
    /**
     * Activate plugin
     */
    public static function on_plugin_activation(){}
    
    /**
     * Deactivate plugin
     */
    public static function on_plugin_deactivation(){}
    
    /**
     * Setup the text domain for localization purposes
     */
    public static function load_plugin_textdomain(){
        load_plugin_textdomain( self::PLUGIN_TEXT_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
    }
    
    /**
     * Remove the widgets from the dashboard screen
     * rif: http://codex.wordpress.org/Function_Reference/remove_meta_box
     * rif: http://www.paulund.co.uk/remove-wordpress-dashboard-widgets
     */
    public static function remove_dashboard_widgets() {
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // Recent Comments
        remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
        remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
        remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
        remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog
        remove_meta_box('dashboard_secondary', 'dashboard', 'side');   // Other WordPress News
        remove_meta_box('dashboard_activity', 'dashboard', 'normal'); // Activity
        remove_action('welcome_panel', 'wp_welcome_panel'); // Welcome Panel
    }
    
    /**
     * Remove Links From WordPress Admin Bar
     * rif. http://www.paulund.co.uk/how-to-remove-links-from-wordpress-admin-bar
     */
    public static function remove_admin_bar_links(){
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('wp-logo'); // Remove the WordPress logo
        $wp_admin_bar->remove_menu('comments'); // Remove Comments
    }
    
    /**
     * Remove menu item from admin
     */
    public static function remove_admin_menu_items() {
        remove_menu_page( 'edit.php' ); // Posts
        remove_menu_page( 'edit-comments.php' ); // Comments
    }
    
    /**
     * Remove default widgets
     * rif. http://www.paulund.co.uk/how-to-remove-default-wordpress-widgets
     */
    public static function remove_default_widgets() {
        unregister_widget( 'WP_Widget_Calendar' );
        unregister_widget( 'WP_Widget_Tag_Cloud' );
        unregister_widget( 'WP_Widget_Archives' );
        unregister_widget( 'WP_Widget_RSS' );
        unregister_widget( 'WP_Widget_Pages' );
        unregister_widget( 'WP_Nav_Menu_Widget' );
	unregister_widget( 'WP_Widget_Recent_Comments' );
	unregister_widget( 'WP_Widget_Recent_Posts' );
        unregister_widget( 'WP_Widget_Search' );
        unregister_widget( 'WP_Widget_Categories' );
        unregister_widget( 'WP_Widget_Meta' );
        unregister_widget( 'WP_Widget_Text' );
    }
    
    /**
     * Remove the Wordpress version
     * rif. http://wplifeguard.com/the-correct-way-to-remove-the-wordpress-version-to-increase-security/
     */
    public static function remove_wp_version(){
        return '';
    }
    
    /**
     * Enable hidden admin featured for displaying all site settings
     */
    public static function add_admin_menu_link_all_options(){
        add_options_page(__('All Settings',self::PLUGIN_TEXT_DOMAIN), __('All Settings',self::PLUGIN_TEXT_DOMAIN), 'administrator', 'options.php');
    }
    
    /**
     * Add all Custom Post Types to at a Glance Dashboard Widget
     * rif. http://www.trickspanda.com/2014/03/add-custom-post-types-glance-dashboard-widget-wordpress/
     */
    public static function custom_dashboard_glance_items(){
        $args = array(
            'public'   => true,
            '_builtin' => false
        );
        $output = 'object';
        $operator = 'and';
        $post_types = get_post_types( $args, $output, $operator );
        foreach ( $post_types as $post_type ) {
            $num_posts = wp_count_posts( $post_type->name );
            $num = number_format_i18n( $num_posts->publish );
            $text = _n( $post_type->labels->singular_name, $post_type->labels->name, intval( $num_posts->publish ) );
            if ( current_user_can( 'edit_posts' ) ) {
                $output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a>';
                echo '<li class="'. $post_type->name . '-count">' . $output . '</li>';
            }
            else {
                $output = '<span>' . $num . ' ' . $text . '</span>';
                echo '<li class="' . $post_type->name . '-count">' . $output . '</li>';
            }
        }
    }
    
    /**
     * Register CSS/JS for ADMIN
     */
    public static function register_css_js_admin(){
        wp_register_style('gre-custom-dashboard', plugins_url('assets/css/gre-custom-dashboard.css', __FILE__), array(), '1.0');
        wp_enqueue_style('gre-custom-dashboard');
    }
    
    /**
     * Hide Advanced Custom Fields Menu
     * rif. http://www.advancedcustomfields.com/resources/how-to-hide-acf-menu-from-clients/
     */
    public static function hide_acf_menu($show){
        return true;
    }
    
    /**
     * Modify the footer text inside of the WordPress admin area.
     * @param type $text
     * rif. http://thomasgriffinmedia.com/blog/2012/08/modify-the-default-admin-footer-text-in-wordpress/
     */
    public static function change_footer_text_admin($text){
        echo sprintf('%s <a href="http://www.spindox.it" target="_blank">Spindox S.p.A.</a> %s <a href="https://wordpress.org/" target="_blank">WordPress.</a>', __('Website developed by', self::PLUGIN_TEXT_DOMAIN), __('Powered by', self::PLUGIN_TEXT_DOMAIN));
    }
    
    /**
     * Allow iframe tag on a multisite
     * rif. http://wpsofa.com/2014/10/allowing-iframe-tags-in-wordpress-multisite-blog-posts/
     * rif. https://github.com/dballari/allow-multisite-iframes-embeds/blob/master/allow-multisite-iframes-embeds.php
     */
    public static function allow_iframe_tags($allowedposttags,$context){
        switch ( $context ) {
            case 'post' :
                $allowedposttags['iframe'] = array(
                    'allowfullscreen' => true,
                    'frameborder' => true,
                    'src' => true,
                    'height' => true,
                    'width' => true,
                    'class' => true,
                    'id' => true,
                    'name' => true,
                    'align' => true,
                    'srcdoc' => true,
                    'seamless' => true,
                );
                return $allowedposttags;
                break;
            default:
                return $allowedposttags;
        }
    }
    
    /**
     * Change WordPress Login Logo
     * rif. http://www.paulund.co.uk/change-wordpress-login-logo-without-a-plugin
     */
    public static function custom_login_head(){
        printf('<style type="text/css">
                    .login h1 a {
                        background-image: url(%s);
                        background-image: none, url(%s);
                        -webkit-background-size: 190px;
                        background-size: 190px;
                        width: 190px;
                        height: 111px;
                        margin: 0 auto;
                    }
                </style>', GRE_CUSTOMDASHBOARD_URL.'/assets/img/spindox-logo.png', GRE_CUSTOMDASHBOARD_URL.'/assets/img/graydee-logo.svg');
    }
    
    /**
     * 
     * @param type $columns
     * @return type
     */
    public static function edit_columns_head_pages($columns){
        $columns = array(
                    'cb'       => '<input type="checkbox" />',
		    'title'    => __( 'Title', self::PLUGIN_TEXT_DOMAIN ),
                    'author'   => __( 'Author', self::PLUGIN_TEXT_DOMAIN ),
                    'template' => __( 'Template Name', self::PLUGIN_TEXT_DOMAIN ),
                    'date'     => __( 'Date', self::PLUGIN_TEXT_DOMAIN )
	);
        return $columns;
    }
    
    /**
     * 
     * @param type $column
     * @param type $page_id
     */
    public static function manage_columns_content_pages($column, $page_id){
        switch( $column ) {
            case 'template' :
                $name_file_template = get_post_meta( $page_id, '_wp_page_template', true );
                $full_path_file_template = get_template_directory() . '/' . $name_file_template;
                $metadata_file = get_file_data($full_path_file_template, array('Template Name' => 'Template Name'));
                echo $metadata_file['Template Name'];
                break;
            default :
                break;
        }
    }
    
    /**
     * Change Login Logo Link URL
     * rif. http://www.paulund.co.uk/customise-wordpress-login-page
     */
    public static function change_login_logo_url(){
        return get_home_url();
    }
    
    /**
     * Change Login Logo URL Title
     * rif. http://www.paulund.co.uk/customise-wordpress-login-page
     * @return string
     */
    public static function change_login_logo_url_title(){
        return 'Graydee';
    }
}
GreCustomDashboard::get_instance();
?>