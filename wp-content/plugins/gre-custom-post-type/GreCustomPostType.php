<?php
/*
Plugin Name: GRE Custom Post Type
Plugin URI: www.spindox.it
Description: Custom Post Type.
Version: 1.0
Author: Spindox S.p.A.
Author URI: www.spindox.it
*/

define( 'GRE_CUSTOMPOSTTYPE_PATH', plugin_dir_path(__FILE__) );
define( 'GRE_CUSTOMPOSTTYPE_URL', plugin_dir_url(__FILE__) );

require_once(__DIR__ .DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'News.php');

class GreCustomPostType{
    
    /*** Refers to a single instance of this class. ***/
    private static $instance = null;
    
    const PLUGIN_TEXT_DOMAIN = 'grecustomposttype';
    
    /**
     * Creates or returns an instance of this class.
     * @return GreCustomPostType a single instance of this class.
     */
    public static function get_instance(){
        if ( null == self::$instance ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Initializes the plugin by setting localization, filters, and administration functions.
     */
    private function __construct() {
        $class_name = get_class();
        register_activation_hook(__FILE__, array( $class_name, 'on_plugin_activation') );
        register_deactivation_hook(__FILE__, array( $class_name, 'on_plugin_deactivation'));
        self::register_hook_callbacks();
        self::load_plugin_textdomain();
    }
    
    /**
     * Register callbacks for actions and filters
     */
    public static function register_hook_callbacks(){
        $class_name = get_class();
        add_action( 'init', array( $class_name, 'init_custom_post_type' ) );
        add_action( 'after_setup_theme', array( $class_name, 'register_image_sizes' ) );
        add_action( 'acf/include_fields', array($class_name, 'register_fields_acf') );
    }
    
    /**
     * Activate plugin
     */
    public static function on_plugin_activation(){}
    
    /**
     * Deactivate plugin
     */
    public static function on_plugin_deactivation(){}
    
    /**
     * Setup the text domain for localization purposes
     */
    public static function load_plugin_textdomain(){
        load_plugin_textdomain( self::PLUGIN_TEXT_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
    }
    
    /**
     * 
     */
    public static function init_custom_post_type(){
        $custom_post_type_options = array( 'context' => self::PLUGIN_TEXT_DOMAIN );
        News::init($custom_post_type_options);
    }
    
    /**
     * 
     */
    public static function register_image_sizes(){}
    
    /**
     * Register fields Advanced Custom fields
     */
    public static function register_fields_acf(){
        if( function_exists('register_field_group') ):
            
        endif;
    }
    
}
GreCustomPostType::get_instance();
?>