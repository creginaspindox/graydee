<?php
require_once('BaseType.php');

abstract class CustomizablePostType extends BaseType {
    
    const CUSTOM_TYPE_PREFIX = 'gre_';
    const CUSTOM_TAXONOMY_NAME = '';
    
    protected function __construct($post){
        
        parent::__construct($post);
	foreach ($post as $key => $value) {
            $this->{$key} = $value;
	}
        
    }
    
    public static function get_custom_post_type_name() {
        return CustomizablePostType::CUSTOM_TYPE_PREFIX . static::CUSTOM_TYPE_NAME;
    }
    
    public static function get_custom_taxonomy_name(){
        return self::CUSTOM_TYPE_PREFIX . static::CUSTOM_TAXONOMY_NAME;
    }
    
    abstract static protected function register_custom_post_type($options = null);
    
    static protected function register_custom_taxonomy($options = null){
        return true;	
    }
    
    static protected function _register_custom_post_type($type_name, $custom_post_type_args = array()){
        
        $defaults = array(
                        'label' => ucfirst($type_name).'s',
			'labels' => array(),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_nav_menus' => true,
			'show_in_menu' => true,
			'show_in_admin_bar' => true,
			'supports' => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'excerpt',
				'trackbacks',
				'custom-fields',
				'comments',
				'revisions',
				'post-formats'
			),
			'taxonomies' => array(
				'category',
				'post_tag'
			),
			'has_archive' => true,
			'rewrite' => array('slug' => $type_name),		
	);
	$custom_post_type_args = wp_parse_args( $custom_post_type_args, $defaults );
	
        return register_post_type( CustomizablePostType::CUSTOM_TYPE_PREFIX . $type_name, $custom_post_type_args );
        
    }
    
    static protected function _register_custom_taxonomy( $taxonomy_name, $associated_types = array(), $custom_args = array() ){
			
        $defaults = array(
			'label' => ucfirst($taxonomy_name).'s',
			'labels' => array(),
			'public' => true,
			'rewrite' => array('slug' => $taxonomy_name)
	);
		
	$custom_args = wp_parse_args( $custom_args, $defaults );
	
        return register_taxonomy( self::CUSTOM_TYPE_PREFIX . $taxonomy_name, $associated_types, $custom_args);
        
    }
    
}
?>