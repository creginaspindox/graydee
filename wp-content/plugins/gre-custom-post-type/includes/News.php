<?php
require_once('CustomizablePostType.php');

class News extends CustomizablePostType {
    
    const CUSTOM_TYPE_NAME = 'news';
    const POST_PER_PAGE = 8;
    
    public function __construct($post) {
        parent::__construct($post);
    }
    
    public static function register_custom_post_type($options = null){
        
        $lang_context = isset($options['context']) ? $options['context'] : null;
        $args = array(
                    'labels' => array(
                                    'name' => __('News', $lang_context),
                                    'singular_name' => __('News', $lang_context),
                                    'add_new' => __('Add News', $lang_context),
                                    'add_new_item' => __('Add a new News', $lang_context),
                                    'edit_item' => __('Edit News', $lang_context),
                                    'new_item' => __('New News', $lang_context),
                                    'all_items' => __('All News', $lang_context),
                                    'view_item' => __('View News', $lang_context),
                                    'search_items' => __('Search News', $lang_context),
                                    'not_found' =>  __('No news found', $lang_context),
                                    'not_found_in_trash' => __('No news found in Trash', $lang_context), 
                                    'parent_item_colon' => '',
                                    'menu_name' => __('News', $lang_context),
                                    'name_admin_bar' => __('News', $lang_context )
                    ),
                    'supports' => array(
                                    'title',
                                    'editor',
                                    'excerpt',
                                    'thumbnail'
                    ),
                    'taxonomies' => array(),
                    'rewrite' => array('slug' => __('news', $lang_context) ),
                    'menu_icon' => 'dashicons-welcome-write-blog'
        );
        
        return parent::_register_custom_post_type( self::CUSTOM_TYPE_NAME, $args );
        
    }
    
    public static function init($options = null){
        self::register_custom_post_type($options);
        $class_name = get_class();
        add_filter( 'enter_title_here', array( $class_name, 'change_default_title' ) );
        add_filter( 'option_posts_per_page', array( $class_name, 'change_post_per_page' ) );
        add_filter( 'manage_'. self::get_custom_post_type_name() .'_posts_columns', array( $class_name, 'edit_columns_head' ) );
        add_action( 'manage_'. self::get_custom_post_type_name() .'_posts_custom_column', array( $class_name, 'manage_columns_content' ), 10, 2 );
    }
    
    public static function get_items($number = -1, $extra_arguments = array()){
        $args = array(
                    'post_status' => 'publish',
                    'post_type'   => self::get_custom_post_type_name(),
                    'posts_per_page'   => $number,
                    'suppress_filters' => false 
        );
        return array_map(function($post){
                            return new News($post);
	}, get_posts(wp_parse_args( $extra_arguments, $args )));
    }
    
    public static function get_items_paged($paged = 1, $number = self::POST_PER_PAGE){
        $extra_arguments = array(
                'paged' => $paged
        );
        $news_list = self::get_items($number, $extra_arguments);
        return $news_list;
    }
    
    public function get_featured_image($size='full'){
        
        $thumbnail_id = get_post_thumbnail_id($this->ID);
        if( empty($thumbnail_id) ){ return false; }
        $args = array (
                   'post_type' => 'attachment',
                   'p'         => $thumbnail_id,
        );
        $thumbnail_details = get_posts($args);
        $thumbnail_src = wp_get_attachment_image_src( $thumbnail_id, $size );
        
        return (object) array(
                           'alt'   => trim(strip_tags( get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true) )),
                           'title' => $thumbnail_details[0]->post_title,
                           'src'   => $thumbnail_src,
                           'caption' => $thumbnail_details[0]->post_excerpt,
                           'description' => $thumbnail_details[0]->post_content
        );
       
    }
    
    public static function change_default_title(){
        $screen = get_current_screen();
        if  ( $screen->post_type == self::get_custom_post_type_name() ) {
            $title = __( 'Enter title news here', GreCustomPostType::PLUGIN_TEXT_DOMAIN );
            return $title;
        }
    }
    
    public static function change_post_per_page($number){
        return ( is_post_type_archive(self::get_custom_post_type_name()) ) ? self::POST_PER_PAGE : $number;
    }
    
    public static function edit_columns_head($columns){
        return $columns;
    }
    
    public static function manage_columns_content($column, $post_id){}
    
    /**
     * Retrieve the permalink for a post type archive.
     */
    public static function get_archive_url(){
        return get_post_type_archive_link(self::get_custom_post_type_name());
    }
    
}
?>