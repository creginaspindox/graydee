/**
 * Created by st.d on 02/03/15.
 */
'use strict';

var map;

var MY_MAPTYPE_ID = 'custom_style';

function initialize() {

    var featureOpts = [
        {
            "stylers": [
                {"visibility": "on"},
                {"saturation": -100}
            ]
        }
    ];

    var mapOptions = {
        zoom: 9,
        center: new google.maps.LatLng(45.338346, 8.479757),
        disableDefaultUI: true,
        scrollwheel: false,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID]
        },
        mapTypeId: MY_MAPTYPE_ID
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    var styledMapOptions = {
        name: 'Custom Style'
    };

    var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

    map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

    setMarkers(map, headquarters);

    google.maps.event.addListener(map, 'click', function () {
        this.setOptions({scrollwheel: true})
    })
}

var headquarters = [
    ['Milano', 45.451867, 9.117560, 1],
    ['Torino', 45.090340, 7.661090, 2]
];

function setMarkers(map, locations) {

    var image = {
        url: GRE.template_url + '/assets/img/marker.png'
    };
    var shape = {
        coords: [1, 1, 1, 20, 18, 20, 18, 1],
        type: 'poly'
    };
    for (var i = 0; i < locations.length; i++) {
        var headquarter = locations[i];
        var myLatLng = new google.maps.LatLng(headquarter[1], headquarter[2]);
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: image,
            //shape: shape,
            title: headquarter[0],
            zIndex: headquarter[3]
        });
    }
}

google.maps.event.addDomListener(window, 'load', initialize);