/**
 * Created by francesco on 09/02/15.
 */

'use strict';

document.onreadystatechange = function (e) {
    if (document.readyState === "complete") onDocumentReady();
};

var header;
var updateCD = function () {
    var now      = new Date(),
        kickoff  = Date.parse("March 23, 2015 18:00:00"),
        diff = kickoff - now;

    var days  = Math.floor( diff / (1000*60*60*24)),
        hours = Math.floor( diff / (1000*60*60) ),
        mins  = Math.floor( diff / (1000*60) ),
        secs  = Math.floor( diff / 1000 );

    var dd = days,
        hh = hours - days  * 24,
        mm = mins  - hours * 60,
        ss = secs  - mins  * 60;

    document.querySelector('.countdown-dh')
        .innerHTML =
        dd + ' days ' +
        hh + ' hours ';
    document.querySelector('.countdown-ms')
        .innerHTML =
        mm + ' minutes ' +
        ss + ' seconds';
};

var onDocumentReady = function () {

    if (document.querySelector('.countdown-dh')) {setInterval(updateCD, 1000)}

    var menu = document.querySelector('#menu');
    var main = document.querySelector('main');
    var search = document.querySelector('#search');
    var close = document.querySelector('#close');
    header = document.querySelector('.header');
    var openMenu = function (e) {
        header.classList.add('open')
    };

    if (menu) {
        menu.addEventListener('click', openMenu, false);
        search.addEventListener('click', openMenu, false);
    }

    var closeMenu = function (e) {
        header.classList.remove('open')
    };

    if (close) {
        close.addEventListener('click', closeMenu, false)
    }

    var lastScrollPosition = main.scrollTop;
    main.addEventListener('scroll', function (e) {
        window.myEvent = e;
        if (lastScrollPosition < main.scrollTop && window.innerHeight > window.innerWidth) {
            if (!document.body.classList.contains('opened')) {
                document.body.classList.add('opened');
            }
        }
        else {
            document.body.classList.remove('opened');
        }
        lastScrollPosition = main.scrollTop;
    }, false);


    //var storyContainer = document.querySelector('#storyContainer');
    //var photoContainer = document.querySelectorAll('.photo-container');
    //var storyClose = document.querySelector('#story-close');
    //
    //var openInfo = function (e) {
    //    storyContainer.classList.add('open-info')
    //};
    //
    //for (var i = 0; i < photoContainer.length; i++) {
    //    var el = photoContainer[i];
    //    el.addEventListener('click', openInfo, false);
    //}
    //
    //// COMMENT  photoContainer.forEach(function(el){
    ////    el.addEventListener('click', openInfo, false);
    ////});COMMENT
    //
    //var closeInfo = function (e) {
    //    console.log(storyContainer);
    //    storyContainer.classList.remove('open-info')
    //};
    //storyClose.addEventListener('click', closeInfo, false);


    var photoContainers = document.querySelectorAll('.photo-container');
    var storyContainers = document.querySelectorAll('.story-container');
    var storyClose = document.querySelectorAll('.story-close');

    for (var i = 0; i < photoContainers.length; i++) {
        var div = photoContainers[i];
        div.addEventListener("click", function (e) {
            e.preventDefault();
            for (var j = 0; j < storyContainers.length; j++) {
                var photo = photoContainers[j];
                var story = storyContainers[j];
                if (photo.classList.contains("active-edit")) {
                    photo.classList.remove('active-edit')
                }
                if (story.classList.contains("open-info")) {
                    story.classList.remove('open-info')
                }
            }
            var divToShow = document.querySelector(this.getAttribute('href'));
            console.log(this, divToShow);
            this.classList.add('active-edit');
            divToShow.classList.add('open-info');
        })
    }

    for (var j = 0; j < storyClose.length; j++) {
        storyClose[j].addEventListener('click', function () {
            for (var l = 0; l < storyContainers.length; l++) {
                if (storyContainers[l].classList.contains("open-info")) {
                    storyContainers[l].classList.remove('open-info')
                }
            }
            for (var k = 0; k < photoContainers.length; k++) {
                if (photoContainers[k].classList.contains("active-edit")) {
                    photoContainers[k].classList.remove("active-edit")
                }
            }
        })
    }

    // TODO: refactoring of this feature
    var collapsibleItems = document.querySelectorAll('.collapsible');

        for (var w = 0; w < collapsibleItems.length; w++) {
                if (location.hash.indexOf(collapsibleItems[w].querySelector('a').getAttribute('name')) > -1) {
                    for (var q = 0; q < collapsibleItems.length; q++) {
                        collapsibleItems[q].classList.add('collapsed')
                    }
                    collapsibleItems[w].classList.remove('collapsed');
                }
        }

    for (var p = 0; p < collapsibleItems.length; p++) {
        collapsibleItems[p].addEventListener('click',function () {
            if (this.classList.contains('collapsed')) {
                for (var k = 0; k < collapsibleItems.length; k++) {
                    collapsibleItems[k].classList.add('collapsed')
                }
                this.classList.remove('collapsed');
            }
        }, false)
    }


    // facebook post ellipsis

    var fbEllipsis = function () {
        String.prototype.trunc = String.prototype.trunc ||
        function(n, useWordBoundary){
            var toLong = this.length > n,
                s_ = toLong ? this.substr(0, n-1) : this;
            s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
            return  toLong ? s_ + '&hellip;' : s_;
        };

        var fbBox = document.querySelectorAll(".text-post-facebook");
        if (fbBox.length > 0) {
            for (var t = 0; t < fbBox.length; t++) {
                fbBox[t].innerHTML = fbBox[t].innerHTML.trunc(210, true);
            }
        }
    };

    fbEllipsis();

    var flipContainers = document.querySelectorAll('.flip-container');

    for ( var r = 0; r < flipContainers.length; r++) {
        flipContainers[r].addEventListener('click', function () {
            if (this.classList.contains('hover')) {
                this.classList.toggle('hover');
            } else {
                for (var f = 0; f < flipContainers.length; f++) {
                    flipContainers[f].classList.remove('hover')
                }
                this.classList.toggle('hover');
            }
        })
    }

};

var refreshRems = function () {
    document.documentElement.style.fontSize = (16 * (document.documentElement.clientWidth / 1600)) + "px";

if (header) {
    header.style.height = "0px";
    header.offsetHeight;
    header.style.height = "";
    header.style.transition = "none";
    header.offsetHeight;
    header.style.transition = "";
}

};
window.addEventListener("resize", refreshRems, false);
refreshRems();