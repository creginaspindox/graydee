<?php get_header(); ?>
<div class="section_blog">
    <div class="blog-block">
        <?php
        $news_list = News::get_items(News::POST_PER_PAGE);
        foreach ($news_list as $news):
            get_template_part('part','one-gre_news');
        endforeach;
        ?>
        <?php GreTheme::get_sidebar('news'); ?>
    </div>
</div>
<?php get_footer(); ?>