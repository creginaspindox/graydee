<?php
global $news;
$title = $news->post_title;
$name_author = get_the_author();
$url = get_permalink($news->ID);
$date_day = date('d', strtotime($news->post_date));
$date_month = date_i18n('M', strtotime($news->post_date));
?>
<div class="blog-item collapsed">
    <div class="blog-date">
        <p class="day"><?php echo $date_day; ?></p>
        <p class="month"><?php echo $date_month; ?></p>
    </div>
    <div class="blog-image">
        <a href="<?php echo $url; ?>">
            <img src="img/128646_1_800.jpg" alt=""/>
        </a>
    </div>
    <?php
    printf('<span class="blog-author">%s %s %s</span>',
            date_i18n(get_option('date_format'), strtotime($news->post_date)),
            __('by', GreTheme::THEME_TEXT_DOMAIN),
            $name_author
    );
    ?>
    <span class="blog-title"><a href="<?php echo $url; ?>"><?php echo $title; ?></a></span>
    <div class="blog-preview">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
        non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>
    <div class="blog-social">
        <img src="img/chat.svg" alt=""/>
        <p>20</p>
        <img src="img/facebook-with-circle.svg" alt=""/>
        <p>22</p>
        <img src="img/linkedin-with-circle.svg" alt=""/>
        <p>28</p>
        <img src="img/twitter-with-circle.svg" alt=""/>
        <p>10</p>
    </div>
</div>