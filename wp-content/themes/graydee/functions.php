<?php
define('GRE_TEMPLATEPATH', get_template_directory());
define('GRE_TEMPLATEURI', get_template_directory_uri());
define('GRE_TEMPLATESECTIONPATH', 'section-templates'.DIRECTORY_SEPARATOR);
define('GRE_TEMPLATESIDEBARPATH', 'sidebar'.DIRECTORY_SEPARATOR);
define('GRE_DIRECTORYNAME_PAGETEMPLATES', 'page-templates');
require_once(GRE_TEMPLATEPATH.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'GreTheme.php');
require_once(GRE_TEMPLATEPATH.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'GreThemeOptions.php');
require_once(GRE_TEMPLATEPATH.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'menu'.DIRECTORY_SEPARATOR.'Gre_Main_Menu_Walker.php');
require_once(GRE_TEMPLATEPATH.DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.'menu'.DIRECTORY_SEPARATOR.'Gre_Footer_Menu_Walker.php');
?>