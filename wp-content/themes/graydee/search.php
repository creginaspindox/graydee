<?php
// http://jamescollings.co.uk/blog/wordpress-search-results-page-modifications/
get_header();
global $wp_query;
$total_results = $wp_query->found_posts;
?>
<div class="search-block">
    <div class="search-form">
        <input type="text" value="<?php echo get_search_query(); ?>" placeholder="TYPE TO SEARCH"/>
        <img src="img/search.svg" class="search-icon right">
    </div>
    <span>
    <?php printf( __( 'Showing %d results for %s', GreTheme::THEME_TEXT_DOMAIN ), $total_results, get_search_query() ); ?> 
    </span>
</div>
<div class="section_search">
    <?php
    if ( have_posts() ) : 
        while (have_posts()) : 
            the_post();
    ?>
    
    <?php
        endwhile; 
    else:
    ?>
    
    <?php
    endif;
    ?>
</div>
<?php get_footer(); ?>