<div class="section_text">
    <?php
    $title = GreTheme::get_field('title_homesectionfollowus', $post->ID);
    $subtitle = GreTheme::get_field('subtitle_homesectionfollowus', $post->ID);
    if( !empty($title) ):
        printf('<h2 class="title">%s</h2>',$title);
    endif;
    echo '<br>';
    if( !empty($subtitle) ):
        printf('<p class="subheading">%s</p>',$subtitle);
    endif;
    ?>
    <div class="row-post">
        <div class="post-facebook">
            <div><img src="<?php echo GRE_TEMPLATEURI.'/assets/img/logo.svg'; ?>" class="logo-post-facebook"></div>
            <h6 class="title-post">GRAYDEE @graydee</h6>
            <div class="text-post-facebook">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id
            ultricies mauris. Suspendisse potenti. Sed volutpat turpis viverra
            #ultricies #graydee
            </div>
            <p class="date-post-facebook">28.Jan.2015 16:30</p>
            <div class="logo-facebook-post">
                <img src="<?php echo GRE_TEMPLATEURI.'/assets/img/facebook_white.svg'; ?>" class="logo-post">
            </div>
        </div>
        <div class="post-container">
            <div class="logo-pinterest-post">
                <img src="<?php echo GRE_TEMPLATEURI.'/assets/img/pinterest_white.svg'; ?>" class="logo-post">
            </div>
            <img class="background-image" src="<?php echo GRE_TEMPLATEURI.'/assets/img/5486801045_5822aab48c_o.jpg'; ?>" alt=""/>
        </div>
        <div class="post-facebook">
            <div><img src="<?php echo GRE_TEMPLATEURI.'/assets/img/logo.svg'; ?>" class="logo-post-facebook"></div>
            <h6 class="title-post">GRAYDEE @graydee</h6>
            <div class="text-post-facebook">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id
            ultricies mauris. Suspendisse potenti. Sed volutpat turpis viverra
            #ultricies #graydee
            </div>
            <span class="ellipsis"></span>
            <p class="date-post-facebook">28.Jan.2015 16:30</p>
            <div class="logo-facebook-post">
                <img src="<?php echo GRE_TEMPLATEURI.'/assets/img/facebook_white.svg'; ?>" class="logo-post">
            </div>
        </div>
    </div>
</div>