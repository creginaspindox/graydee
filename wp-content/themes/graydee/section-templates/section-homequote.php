<?php
$text = GreTheme::get_field('text_homesectionquote', $post->ID);
if ( !empty($text) ):
?>
    <div class="small-section">
        <div class="preface-container" id="peak">
            <div class="outer-center">
                <h4><?php echo $text; ?></h4>
                <div class="inner-center"></div>
            </div>
        </div>
    </div>
<?php
endif;
?>