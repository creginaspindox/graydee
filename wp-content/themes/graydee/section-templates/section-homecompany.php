<?php
$title = GreTheme::get_field('tile_homesectioncompany', $post->ID);
$subtitle = GreTheme::get_field('subtitle_homesectioncompany', $post->ID);
$label_button = GreTheme::get_field('labelbutton_homesectioncompany', $post->ID);
$url_button = GreTheme::get_field('linkbutton_homesectioncompany', $post->ID);
if( !empty($title) || !empty($subtitle) || (!empty($label_button) && !empty($url_button)) ):
?>
    <div class="section">
        <div class="overlapping">
            <div class="vertical-big left"></div>
            <div class="horizontal-double right"></div>
            <div class="horizontal-small right"></div>
            <div class="horizontal-small bckgrnd-photo right"></div>
            <div class="color-mask"></div>
        </div>
        <div class="overlapping-upper">
            <?php
            if( !empty($title) ):
                printf('<h1 class="competence">%s</h1>',$title);
            endif;
            if( !empty($subtitle) ):
                printf('<p class="second-title">%s</p>',$subtitle);
            endif;
            if( !empty($label_button) && !empty($url_button) ):
                printf('<div class="alt-btn_container">
                            <a href="%s"><button type="button" class="alt-btn">%s</button></a>
                        </div>', $url_button, $label_button);
            endif;
            ?>
        </div>
    </div>
<?php
endif;
?>
