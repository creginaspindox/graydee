<div class="section-map" style="background-color:black">
    <div id="map-canvas"></div>
    <div class="headquarters">
        <div>
            <img src="<?php echo GRE_TEMPLATEURI.'/assets/img/logo_brown.svg'; ?>" class="logo-post-map">
        </div>
        <?php
        GreTheme::print_location_google_map();
        ?>
    </div>
</div>