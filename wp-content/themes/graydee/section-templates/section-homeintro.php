<?php
$intro_title = GreTheme::get_field('introtitle_homesectionintro', $post->ID);
$title = GreTheme::get_field('title_homesectionintro', $post->ID);
$label_button = GreTheme::get_field('labelbutton_homesectionintro', $post->ID);
$url_button = GreTheme::get_field('linkbutton_homesectionintro', $post->ID);
?>
<div class="container">
    <hgroup class="main_title_container">
        <?php
        if( !empty( $intro_title ) ):
            printf('<h3>%s</h3>',$intro_title);
        endif;
        if( !empty( $title ) ):
            printf('<h1>%s</h1>',$title);
        endif;
        if( !empty( $label_button ) && !empty( $url_button ) ):
            printf('<a href="%s"><button type="button" class="btn_explore">%s</button></a>',
                    $url_button, $label_button);
        endif;
        ?>
    </hgroup>
</div>