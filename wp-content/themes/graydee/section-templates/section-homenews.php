<?php
$title = GreTheme::get_field('title_homesectionnews', $post->ID);
$subtitle = GreTheme::get_field('subtitle_homesectionnews', $post->ID);
$label_button = GreTheme::get_field('labelbutton_homesectionnews', $post->ID);
$url_button = News::get_archive_url();
$number_news = GreTheme::get_field('number_homesectionnews', $post->ID);
?>
<div class="section_text">
    <div>
        <?php
        if( !empty($label_button) ):
            printf('<a href="%s"><button type="button" class="btn_explore right">%s</button></a>', $url_button, $label_button);
        endif;
        ?>
        <?php
        if( !empty($title) ):
            printf('<h2 class="title">%s</h2>',$title);
        endif;
        ?>
        <br>
        <?php
        if( !empty($subtitle) ):
            printf('<p class="subheading">%s</p>',$subtitle);
        endif;
        ?>
    </div>
    <?php
    $news_list = News::get_items($number_news);
    foreach ($news_list as $news):
        get_template_part('loop','news_home');
    endforeach;
    ?>
</div>