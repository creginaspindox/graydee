<?php
$title = GreTheme::get_field('tile_homesectionevents', $post->ID);
$label_button = GreTheme::get_field('labelbutton_homesectionevents', $post->ID);
$url_button = GreTheme::get_field('linkbutton_homesectionevents', $post->ID);
if( !empty($title) || (!empty($label_button) && !empty($url_button)) ):
?>
    <div class="section">
        <div class="overlapping">
            <div class="vertical left"></div>
            <div class="horizontal right"></div>
            <div class="horizontal-half right"></div>
            <div class="horizontal-half-small right"></div>
        </div>
        <div class="overlapping-default">
            <?php
            if( !empty($title) ):
                printf('<h1 id="event">%s</h1>',$title);
            endif;
            ?>
        </div>
        <div class="btn_container">
            <?php
            if( !empty($label_button) && !empty($url_button) ):
                printf('<a href="%s"><button type="button" class="btn_explore">%s</button></a>',
                        $url_button, $label_button);
            endif;
            ?>
        </div>
    </div>
<?php
endif;
?>