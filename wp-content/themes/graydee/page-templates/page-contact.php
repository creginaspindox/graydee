<?php
/**
 * Template Name: Template Page Contact
 */
?>
<?php get_header(); ?>
<?php GreTheme::get_section_template('googlemap'); ?>
<div class="contact-block">
    <div class="form-container">
        <h2 class="title">contact us</h2>
        <p>For all inquiries please contact</p>
        <p class="contact-email">info@graydee.com</p>
        <form class="contact-form">
            <label for="name"><?php _e('Name',GreTheme::THEME_TEXT_DOMAIN); ?> *</label>
            <input id="name" class="feedback-input" type="text" placeholder="<?php _e('First name',GreTheme::THEME_TEXT_DOMAIN); ?>" />
            <input id="surname" class="feedback-input" type="text" placeholder="<?php _e('Last name',GreTheme::THEME_TEXT_DOMAIN); ?>" />
            <label for="email"><?php _e('Email Address',GreTheme::THEME_TEXT_DOMAIN); ?> *</label>
            <input id="email" class="feedback-input" type="email" />
            <label for="subject"><?php _e('Subject',GreTheme::THEME_TEXT_DOMAIN); ?> *</label>
            <input id="subject" class="feedback-input" type="text"/>
            <label for="message"><?php _e('Message',GreTheme::THEME_TEXT_DOMAIN); ?> *</label>
            <textarea class="feedback-input" id="message" type="text"></textarea>
            <button type="button" class="btn_explore"><?php _e('submit',GreTheme::THEME_TEXT_DOMAIN); ?></button>
        </form>
    </div>
</div>
<?php get_footer(); ?>