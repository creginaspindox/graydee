<?php
/**
 * Template Name: Template Home Page
 */
?>
<?php
get_header('home');
GreTheme::get_section_template('homequote');
GreTheme::get_section_template('homeevents');
GreTheme::get_section_template('homenews');
GreTheme::get_section_template('homecompany');
GreTheme::get_section_template('homefollowus');
GreTheme::get_section_template('googlemap');
get_footer();
?>