<?php
class GreTheme{
    
    /*** Refers to a single instance of this class. ***/
    private static $instance = null;
    
    const THEME_TEXT_DOMAIN = 'graydee';
    const FILENAME_PAGE_TEMPLATE_HOME = 'page-home.php';
    const FILENAME_PAGE_TEMPLATE_CONTACT = 'page-contact.php';
    const FILENAME_PAGE_TEMPLATE_COMPANY = 'page-company.php';
    
    /**
     * Creates or returns an instance of this class.
     * @return GreTheme a single instance of this class.
     */
    public static function get_instance(){
        if ( null == self::$instance ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Initializes the plugin by setting localization, filters, and administration functions.
     */
    private function __construct() {
        self::register_hook_callbacks();
    }
    
    /**
     * Register callbacks for actions and filters
     */
    public static function register_hook_callbacks(){
        $class_name = get_class();
        add_action( 'after_setup_theme', array( $class_name, 'setup_theme' ) );
        add_action( 'wp_enqueue_scripts', array( $class_name, 'register_css_js' ) );
        add_action( 'wp_head', array( $class_name, 'print_google_analytics' ) );
        add_action( 'wp_head', array( $class_name, 'head_js' ) );
        add_action( 'init', array( $class_name, 'remove_head_links' ) );
        add_action( 'admin_init', array( $class_name, 'remove_feature') );
        add_action( 'widgets_init', array( $class_name, 'register_sidebar' ) );
    }
    
    /**
     * Set up theme defaults and registers support for various WordPress features.
     */
    public static function setup_theme(){
        load_theme_textdomain( self::THEME_TEXT_DOMAIN, GRE_TEMPLATEPATH. '/languages' );
        add_theme_support( 'post-thumbnails' ); // This feature enables Post Thumbnails support for a Theme
        register_nav_menus( array(
		'main'   => __( 'Main Menu', self::THEME_TEXT_DOMAIN ),
		'footer' => __( 'Footer Menu', self::THEME_TEXT_DOMAIN )
	));
    }
    
    /**
     * Register scripts and css for the front end.
     */
    public static function register_css_js(){
         if( !is_admin() ):
            wp_deregister_script('jquery');
            wp_register_script('jquery', GRE_TEMPLATEURI . '/assets/js/jquery.min.js', array(), '1.11.2', false);
            wp_enqueue_script('jquery');
            wp_register_script('app', GRE_TEMPLATEURI . '/assets/js/app.js', array(), '1.0', false);
            wp_enqueue_script('app');
            wp_register_script('googlemap', 'https://maps.googleapis.com/maps/api/js', array(), '3.0', false);
            wp_register_script('map', GRE_TEMPLATEURI . '/assets/js/map.js', array(), '1.0', false);
            wp_register_style('style', GRE_TEMPLATEURI . '/assets/css/style.css', array(), '1.0', 'all');
            wp_enqueue_style('style');
            if( is_page_template(self::get_templatepage_directory_uri().self::FILENAME_PAGE_TEMPLATE_HOME) ):
                wp_enqueue_script('googlemap');
                wp_enqueue_script('map');
            endif;
            if( is_page_template(self::get_templatepage_directory_uri().self::FILENAME_PAGE_TEMPLATE_CONTACT) ):
                wp_enqueue_script('googlemap');
                wp_enqueue_script('map');
                wp_register_script('jquery.validate', GRE_TEMPLATEURI . '/assets/js/jquery.validate.min.js', array('jquery'), '1.13.1', false);
                wp_enqueue_script('jquery.validate');
                wp_register_script('form-contact', GRE_TEMPLATEURI . '/assets/js/form-contact.js', array('jquery','jquery.validate'), '1.0', false);
                wp_enqueue_script('form-contact');
            endif;
        endif;
    }
    
    /**
     * Display tracking code Google Analytics into the page
     * Only user is not logged in
     */
    public static function print_google_analytics(){}
    
    /**
     * Remove code from the <head>
     * rif. http://wpsmackdown.com/wordpress-cleanup-wp-head/
     */
    public static function remove_head_links(){
        remove_action('wp_head', 'wlwmanifest_link'); // Remove the link to the Windows Live Writer manifest file.
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0); // Remove shortlink
        remove_action('wp_head', 'rel_canonical'); // Remove canonical links
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); //Remove previous/next post links
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'rsd_link');
        remove_action( 'wp_head', array($GLOBALS['sitepress'], 'meta_generator_tag' ) ); //Remove meta tag generator plugin WPML
    }
    
    /**
     * Retrieve page template directory URI.
     * @return string Page template directory URI.
     */
    public static function get_templatepage_directory_uri(){
        return GRE_DIRECTORYNAME_PAGETEMPLATES . '/';
    }
    
    /**
     * Remove support of features from a post type
     */
    public static function remove_feature(){
        $post_id = isset($_GET['post']) ? $_GET['post'] : ( isset($_POST['post_ID']) ? $_POST['post_ID'] : null );
        if( empty( $post_id ) ) return;
        $template_file = get_post_meta($post_id, '_wp_page_template', true);
        switch ( $template_file ):
            case self::get_templatepage_directory_uri() . self::FILENAME_PAGE_TEMPLATE_HOME :
                remove_post_type_support('page', 'editor');
                remove_post_type_support('page', 'thumbnail');
                remove_post_type_support('page', 'comments');
                remove_post_type_support('page', 'custom-fields');
                break;
            case self::get_templatepage_directory_uri() . self::FILENAME_PAGE_TEMPLATE_CONTACT :
                remove_post_type_support('page', 'thumbnail');
                remove_post_type_support('page', 'comments');
                remove_post_type_support('page', 'custom-fields');
                break;
            case self::get_templatepage_directory_uri() . self::FILENAME_PAGE_TEMPLATE_COMPANY :
                remove_post_type_support('page', 'thumbnail');
                remove_post_type_support('page', 'comments');
                remove_post_type_support('page', 'custom-fields');
                break;
            default:
        endswitch;
    }
    
    /**
     * Register sidebar
     */
    public static function register_sidebar(){
        register_sidebar( array(
                'name'          => __( 'Sidebar News', self::THEME_TEXT_DOMAIN ),
		'id'            => 'sidebar-news',
		'description'   => __( 'Sidebar that appears on the right.', self::THEME_TEXT_DOMAIN ),
		'before_widget' => '<div class="sidebar-container">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="sidebar-title"><h6>',
		'after_title'   => '</h6></div>',
        ));
    }
    
    /**
     * Get the list of languages
     * @return type
     */
    public static function get_all_languages(){
        return icl_get_languages('skip_missing=0&orderby=code');
    }
    
    /**
     * 
     */
    public static function print_languages(){
        $languages = self::get_all_languages();
        if( !empty($languages) ):
            echo '<ul class="lang_list">';
            foreach( $languages as $language ):
                printf('<li class="language %s"><a href="%s">%s</a></li>',
                        !empty($language['active']) ? 'active' : '',
                        $language['url'],
                        strtoupper($language['language_code'])
                );
            endforeach;
            echo '</ul>';
        endif;
    }
    
    /**
     * 
     */
    public static function print_logo(){
        if ( is_front_page() ):
            printf('<img src="%s" class="logo-home" />',
                    GRE_TEMPLATEURI.'/assets/img/logo.svg');
        else:    
            printf('<a href="%s"><img src="%s" class="logo"></a>',
                    home_url(), GRE_TEMPLATEURI.'/assets/img/logo_brown.svg');
        endif;
    }
    
    /**
     * 
     * @return type
     */
    public static function get_main_menu(){
        $main_menu_args = array(
                'theme_location'  => 'main',
                'echo'            => false,
                'container'       => 'nav',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => 'menu-container',
                'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                'walker'          => new Gre_Main_Menu_Walker()
        );
        $main_menu = wp_nav_menu($main_menu_args);
        return $main_menu;
    }
    
    public static function print_main_menu(){
        echo self::get_main_menu();
    }
    
    /**
     * 
     * @return type
     */
    public static function get_footer_menu(){
        $footer_menu_args = array(
                'theme_location'  => 'footer',
                'echo'            => false,
                'container'       => '',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => 'info-footer',
                'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                'walker'          => new Gre_Footer_Menu_Walker()
        );
        $footer_menu = wp_nav_menu($footer_menu_args);
        return $footer_menu;
    }
    
    public static function print_footer_menu(){
        echo self::get_footer_menu();
    }
    
    /**
     * 
     */
    public static function print_header_home_social_icons(){
        $social_icons = GreThemeOptions::get_social_options();
        echo '<ul class="social-list">';
        foreach ( $social_icons as $key => $value ):
            printf('<li class="li_social">
                        <a href="%s"><img src="%s_white.svg" class="social"></a>
                    </li>', $value, GRE_TEMPLATEURI.'/assets/img/'.$key
            );
        endforeach;
        echo '</ul>';
    }
    
    public static function print_header_social_icons(){
        $social_icons = GreThemeOptions::get_social_options();
        echo '<ul class="social-list">';
        foreach ( $social_icons as $key => $value ):
            printf('<li class="li_social">
                        <a href="%s"><img src="%s_darkgrey.svg" class="social"></a>
                    </li>', $value, GRE_TEMPLATEURI.'/assets/img/'.$key
            );
        endforeach;
        echo '</ul>';
    }
    
    /**
     * 
     */
    public static function print_footer_social_icons(){
        $social_icons = array_filter(GreThemeOptions::get_social_options());
        $label_social = GreThemeOptions::get_footer_options();
        if ( !empty($label_social) ):
            printf('<h6 class="social-footer-container">%s</h6>',$label_social);
        endif;
        if( !empty($social_icons) ):
            echo '<ul class="social-footer">';
            foreach ( $social_icons as $key => $value ):
                printf('<li class="li-social-footer">
                            <a href="%s"><img src="%s_white.svg" class="icon-social"></a>
                        </li>', $value, GRE_TEMPLATEURI.'/assets/img/'.$key
                );
            endforeach;
        echo '</ul>';
        endif;
    }
    
    /**
     * 
     */
    public static function print_header_home_action_link(){
        printf('<ul class="action-link right">
                    <li class="li-link right"><img src="%s" class="action" id="menu"></li>
                    <li class="li-link right"><img src="%s" class="action" id="search"></li>
                </ul>', GRE_TEMPLATEURI.'/assets/img/menu.svg', GRE_TEMPLATEURI.'/assets/img/search.svg');
    }
    
    public static function print_header_action_link(){
        printf('<ul class="action-link right">
                    <li class="li-link right"><img src="%s" class="action" id="menu"></li>
                    <li class="li-link right"><img src="%s" class="action" id="search"></li>
                </ul>', GRE_TEMPLATEURI.'/assets/img/menu_darkgrey.svg', GRE_TEMPLATEURI.'/assets/img/search_darkgrey.svg');
    }
    
    /**
     * Retrieve search template
     */
    public static function get_search_form($name = null){
        switch ( $name ):
            case 'news':
                get_template_part('searchform', 'news');
                break;
            case 'home':
                get_template_part('searchform', 'home');
                break;
            default:  
        endswitch;
    }
    
    /**
     * Retrieve section template
     */
    public static function get_section_template($name = null){
        switch ( $name ):
            case 'homeintro':
                get_template_part(GRE_TEMPLATESECTIONPATH.'section', 'homeintro');
                break;
            case 'homequote':
                get_template_part(GRE_TEMPLATESECTIONPATH.'section', 'homequote');
                break;
            case 'homeevents':
                get_template_part(GRE_TEMPLATESECTIONPATH.'section', 'homeevents');
                break;
            case 'homenews':
                get_template_part(GRE_TEMPLATESECTIONPATH.'section', 'homenews');
                break;
            case 'homecompany':
                get_template_part(GRE_TEMPLATESECTIONPATH.'section', 'homecompany');
                break;
            case 'homefollowus':
                get_template_part(GRE_TEMPLATESECTIONPATH.'section', 'homefollowus');
                break;
            case 'googlemap':
                get_template_part(GRE_TEMPLATESECTIONPATH.'section', 'googlemap');
                break;
            default:
        endswitch;
    }
    
    public static function get_sidebar($name = null){
        switch ( $name ):
            case 'news':
                get_template_part(GRE_TEMPLATESIDEBARPATH.'sidebar', 'news');
                break;
            default:
        endswitch;
    }
    
    public static function head_js(){
        printf('<script type="text/javascript">
                    //<![CDATA[
                    ;(function(window,undefined){
                                window.GRE = window.GRE || {}
                                window.GRE.ajax_url = \'%s\';
                                window.GRE.template_url = \'%s\';
                    })(window);
                    //]]>
                </script>', admin_url( 'admin-ajax.php'), GRE_TEMPLATEURI
        );
    }
    
    /**
     * 
     */
    public static function print_location_google_map(){
        $locations = GreThemeOptions::get_locations_options();
        printf('<div class="city-container">
                    <h6 class="city">%s</h6>
                    <p class="address">%s</p>
                    <p class="address">%s</p>
                </div>', $locations['location1']['city'],
                         $locations['location1']['address'],
                         $locations['location1']['cap-and-city']
        );
        printf('<div class="city-container">
                    <h6 class="city">%s</h6>
                    <p class="address">%s</p>
                    <p class="address">%s</p><br/>
                    <p class="address">%s</p>
                    <p class="address">%s</p>
                </div>', $locations['location2']['city'],
                         $locations['location2']['address'],
                         $locations['location2']['cap-and-city'],
                         $locations['infodata']['email'],
                         $locations['infodata']['phone']
        );
    }
    
    public static function get_field($key,$post_id = false){
        return get_field($key,$post_id);
    }
}
GreTheme::get_instance();
?>