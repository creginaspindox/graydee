<?php
class GreThemeOptions{
    
    /*** Refers to a single instance of this class. ***/
    private static $instance = null;
    
    /**
     * Creates or returns an instance of this class.
     * @return GreThemeOptions a single instance of this class.
     */
    public static function get_instance(){
        if ( null == self::$instance ) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Initializes the plugin by setting localization, filters, and administration functions.
     */
    private function __construct() {
        self::register_hook_callbacks();
    }
    
    /**
     * Register callbacks for actions and filters
     */
    public static function register_hook_callbacks(){
        $class_name = get_class();
        add_action( 'after_setup_theme', array( $class_name, 'register_options_page' ) );
        add_action( 'acf/include_fields', array($class_name, 'register_fields_acf') );
    }
    
    /**
     * Register option page with ACF
     */
    public static function register_options_page(){
        if( function_exists('acf_add_options_page') ):
                $page = acf_add_options_page(array(
                        'page_title' 	=> __('Options', GreTheme::THEME_TEXT_DOMAIN),
                        'menu_title' 	=> __('Options', GreTheme::THEME_TEXT_DOMAIN),
                        'menu_slug' 	=> 'gre-options',
                        'capability' 	=> 'edit_posts',
                        'redirect' 	=> false
                ));
        endif;
    }
    
    /**
     * Register fields Advanced Custom fields
     */
    public static function register_fields_acf(){
        if( function_exists('register_field_group') ):
            
        endif;
    }
    
    /**
     * Wrapper for retrieving values from the options page with Advanced Custom field
     * @param type $field_name
     * @return type
     */
    public static function get_option($field_name){
        $option_value = get_field($field_name, 'option');
        return !empty($option_value) ? $option_value : null;
    }
    
    public static function get_social_options(){
        return array(
                   'facebook'    => self::get_option('facebook_url'),
                   'twitter'     => self::get_option('twitter_url'),
                   'instagram'   => self::get_option('instagram_url'),
                   'pinterest'   => self::get_option('pinterest_url'),
                   'linkedin'    => self::get_option('linkedin_url')
        );
    }
    
    public static function get_locations_options(){
        return array(
                'location1' => array(
                                'city'         => self::get_option('city_location1'),
                                'address'      => self::get_option('address_location1'),
                                'cap-and-city' => self::get_option('cap_and_city_location1')
                ),
                'location2' => array(
                                'city'         => self::get_option('city_location2'),
                                'address'      => self::get_option('address_location2'),
                                'cap-and-city' => self::get_option('cap_and_city_location2')
                ),
                'infodata' => array(
                                'email' => self::get_option('email_infodata'),
                                'phone' => self::get_option('phone_infodata')
                )
        );
    }
    
    public static function get_footer_options(){
        return self::get_option('label_social_network_footer');
    }
    
}
GreThemeOptions::get_instance();
?>