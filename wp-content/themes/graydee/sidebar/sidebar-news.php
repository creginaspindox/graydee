<?php if ( is_active_sidebar( 'sidebar-news' ) ) : ?>
        <div class="blog-sidebar">
            <?php dynamic_sidebar( 'sidebar-news' ); ?>
        </div>
<?php endif; ?>
