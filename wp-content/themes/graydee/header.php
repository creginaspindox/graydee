<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
</head>
<body>
    <main>
        <header class="header">
            <div class="header-closed">
                <div class="link-container">
                    <?php
                    GreTheme::print_languages();
                    GreTheme::print_header_social_icons();
                    GreTheme::print_header_action_link();
                    ?>
                </div>
                <?php GreTheme::print_logo(); ?>
            </div>
            <aside>
                <?php GreTheme::print_main_menu(); ?>
                <div>
                    <img src="<?php echo GRE_TEMPLATEURI.'/assets/img/close.svg'; ?>" class="close" id="close">
                </div>
                <div class="search">
                    <div class="search-elements">
                        <?php GreTheme::get_search_form('home'); ?>
                    </div>
                </div>
                <div class="close-btn"></div>
            </aside>
        </header>