<article>
        <div class="news-photo">
            <div class="news-date-container">
                <div class="news-date">
                    <div class="day">
                        <p class="news-date-text">15</p>
                    </div>
                    <div class="month">
                        <p class="news-date-text">feb</p>
                    </div>
                </div>
                <div class="news-date-mask"></div>
            </div>
            <img src="img/4579620162_60435f8052_o.jpg" alt=""/>
        </div>
        <div class="news-text right">
            <h5 class="title-article">Lorem ipsum blablablablabla</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id ultricies mauris. Suspendisse
                    potenti. Sed volutpat turpis viverra sapien ultricies accumsan. Etiam nec felis faucibus, sagittis
                    est eget, lacinia ipsum. tempor ante. Morbi efficitur venenatis erat sodales vulputate.</p>
        </div>
    </article>