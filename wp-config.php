<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file definisce le seguenti configurazioni: impostazioni MySQL,
 * Prefisso Tabella, Chiavi Segrete, Lingua di WordPress e ABSPATH.
 * E' possibile trovare ultetriori informazioni visitando la pagina: del
 * Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Editing wp-config.php}. E' possibile ottenere le impostazioni per
 * MySQL dal proprio fornitore di hosting.
 *
 * Questo file viene utilizzato, durante l'installazione, dallo script
 * di creazione di wp-config.php. Non � necessario utilizzarlo solo via
 * web,� anche possibile copiare questo file in "wp-config.php" e
 * rimepire i valori corretti.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - E? possibile ottenere questoe informazioni
// ** dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'graydeedev');

/** Nome utente del database MySQL */
define('DB_USER', 'root');

/** Password del database MySQL */
define('DB_PASSWORD', 'lgnkr27');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8');

/** Il tipo di Collazione del Database. Da non modificare se non si ha
idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * E' possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * E' possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ci� forzer� tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HlfMIG_mG3#,+g Nqz61bw*lK:z4Y>BZs.v;-y@V+i37PA!Rbv&1Cy6y?/Q^]9RO');
define('SECURE_AUTH_KEY',  'aMD6XOj-QY3g}Pj@68HK|:ila8J>iW$SJL7qa.kXl)*33vyn?1/4F,yXm[44n=^3');
define('LOGGED_IN_KEY',    'l{?}@VB[F4)5M8_Jw5=A,C;]]`/hl.8K-+e$a)M%aEwgH(g*,G.`b{:%0mr D7;@');
define('NONCE_KEY',        'A0(,ez[IUkbKQvs%z<&./J9uJ-M#p]{U1m #qX|eA44vN6&[/wJ20m3AaM|Z{VQ4');
define('AUTH_SALT',        '`fh584]}cREV:J@6HK;OaJ]u}~fAjkUin?2,kMs%Y}h+r9cya^FGiXes6Z*GkeL-');
define('SECURE_AUTH_SALT', 'j5R#?0Jz|;iZawbW[t`XY5<l_XsVMHmD`XB:fv-~yobD}rklr?%NKI,C( O;?/J7');
define('LOGGED_IN_SALT',   '2:/by#1TXcz_k0z)LxjOD.9rdFDRr9q6QC=9L<_>([[.rQ{4P5,hQy^z8!5ddOb6');
define('NONCE_SALT',       '+dgeTxEZ~NyC*p9[@7-GF8>s)S7a1RALX.qf.>s#$>:F}B[1R2<0(Q9RDjiYR~79');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress .
 *
 * E' possibile avere installazioni multiple su di un unico database if you give each a unique
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wpgre_';

/**
 * Per gli sviluppatori: modalit� di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * E' fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all'interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', true);
define('WP_DEBUG_LOG', true);
define('SCRIPT_DEBUG', false);
define('SAVEQUERIES', true);

/*** Disabilita il CRON ***/
define('DISABLE_WP_CRON', true);

/*** Limito il numero di Revision ***/
define('WP_POST_REVISIONS', 3);

/*** Modify AutoSave Interval ***/
define( 'AUTOSAVE_INTERVAL', 180);

/*** Disable the Plugin and Theme Editor ***/
define('DISALLOW_FILE_EDIT', true);

/*** Disable Plugin and Theme Update and Installation ***/
define('DISALLOW_FILE_MODS', true);

/*** Disable all automatic updates ***/
define('AUTOMATIC_UPDATER_DISABLED', true);

/*** Disabilito CSS a JS per il plugin WPML ***/
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta lle variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
